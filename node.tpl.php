<?php if (!$page): ?>
  <a href="<?php print $node_url ?>" title="<?php print $title ?>">
		<?php if ($node->field_image[0]): ?>
  		<?php print $node->field_image[0]['view'] ?>
		<?php endif; ?>
  	<h3><?php print $title ?></h3>
    <p><?php print strip_tags($content); ?></p>
  </a>
<?php else: ?>

  <div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
  
  <?php print $picture ?>
  
    <div class="meta">
    <?php if ($submitted): ?>
      <span class="submitted"><?php print $submitted ?></span>
    <?php endif; ?>
  
    <?php if ($terms): ?>
      <div class="terms terms-inline"><?php print $terms ?></div>
    <?php endif;?>
    </div>
  
    <div class="content">
      <?php print $content ?>
    </div>
  
    <?php print $links; ?>
  </div>

<?php endif; ?>

