<?php
/*********************************************************
*
*		project: jqmobile
*
*
*	Remove all js & css of Drupal core & modules.
*
*********************************************************/
 
function jqmobile_preprocess_page(&$variables) {

	//remove other css
	$css = drupal_add_css();
	$new_css = array();
	$new_css['all']['theme'] = $css['all']['theme'];
	$variables['styles'] = drupal_get_css($new_css);
	
	//remove other js
	$scripts = drupal_add_js();
	$mobile_scripts = drupal_get_js('header',array('theme'=>$scripts['theme']));
//	print_r($new_scripts);
	unset($variables['scripts']);
	$variables['mobile_scripts'] = $mobile_scripts;

	//Replace Primary-link by a custom menu:"mobile-primary" when available.	
	$primary = menu_navigation_links('menu-mobile-primary', $level = 0);
	if ($primary) {
		$variables['primary_links'] = $primary;
	}
	
}

function jqmobile_views_mini_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
	global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.


  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹‹')), $limit, $element, 1, $parameters);
  if (empty($li_previous)) {
    $li_previous = "&nbsp;";
  }

  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('››')), $limit, $element, 1, $parameters);
  if (empty($li_next)) {
    $li_next = "&nbsp;";
  }

  if ($pager_total[$element] > 1) {
    $items[] = array(
      'class' => 'pager-previous', 
      'data' => $li_previous,
    );

    $items[] = array(
      'class' => 'pager-current', 
//      'data' => t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)),
				'data'	=> '<input type="button" data-role="button" disabled="disabled" value="'.t('@current of @max', array('@current' => $pager_current, '@max' => $pager_max)).'" />',
    );

    $items[] = array(
      'class' => 'pager-next', 
      'data' => $li_next,
    );
//    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
		$output = '';
		foreach($items as $item) {
			$output .= $item['data'];
		}
		$output = '<div data-role="controlgroup" data-type="horizontal">'.$output.'</div>';
		return $output;
  }
	
}

function jqmobile_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'), 
        t('‹ previous') => t('Go to previous page'), 
        t('next ›') => t('Go to next page'), 
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

	//set attributes data-role etc.
	$attributes['data-role'] = 'button';
	$attributes['data-inline'] = 'true';

  return l($text, $_GET['q'], array('attributes' => $attributes, 'query' => count($query) ? implode('&', $query) : NULL));
	
}

function jqmobile_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
	$quantity = 1;
	
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('« first')), $limit, $element, $parameters);
  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : t('‹ previous')), $limit, $element, 1, $parameters);
  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : t('next ›')), $limit, $element, 1, $parameters);
  $li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last »')), $limit, $element, $parameters);

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first', 
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous', 
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
/*      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis', 
          'data' => '…',
        );
      }*/
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item', 
            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current', 
//            'data' => $i,
						'data'	=> '<input type="button" data-role="button" disabled="disabled" value="'.$i.'" />',
					);
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item', 
            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
/*      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis', 
          'data' => '…',
        );
      }*/
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => 'pager-next', 
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => 'pager-last', 
        'data' => $li_last,
      );
    }
//    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
		//start hook:
		$output = '';
		foreach($items as $item) {
			$output .= $item['data'];
		}
		$output = '<div data-role="controlgroup" data-type="horizontal">'.$output.'</div>';
		return $output;
  }
}

/*	theme_item_list
*	required
*/
function jqmobile_item_list($items = array(), $title = NULL, $type = 'ul', $attributes = NULL) {
	//add
	$attributes['data-role'] = 'listview';
	$attributes['data-inset'] = 'true';
	
  $output = '<div class="item-list">';
  if (isset($title)) {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        $data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
      }
      if ($i == 0) {
        $attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] . ' first');
      }
      if ($i == $num_items - 1) {
        $attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] . ' last');
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

/*
*	theme_comment_block
*/
function jqmobile_comment_block() {
  $items = array();
  foreach (comment_get_recent() as $comment) {
		$str = '<h3>'.$comment->subject.'</h3>';
		$str .= '<p>'.t('@time ago', array('@time' => format_interval(time() - $comment->timestamp))).'</p>';
//    $items[] = l($comment->subject, 'node/' . $comment->nid, array('fragment' => 'comment-' . $comment->cid)) . '<br />' . t('@time ago', array('@time' => format_interval(time() - $comment->timestamp)));
    $items[] = l($str, 'node/' . $comment->nid, array('html' => TRUE, 'fragment' => 'comment-' . $comment->cid));
	}
  if ($items) {
    return theme('item_list', $items);
  }
}


/*
*	theme_node_submitted
*/
function jqmobile_node_submitted($node) {
  return t('!username @datetime', 
    array(
    '!username' => theme('username', $node), 
    '@datetime' => format_date($node->created),
  ));
}

function jqmobile_menu_tree($tree) {
  return '<ul class="menu" data-role="listview" data-inset="true">' . $tree . '</ul>';
}

function jqmobile_more_link($url, $title) {
  return '<div class="more-link">' . t('<a href="@link" title="@title" data-role="button">more</a>', array('@link' => check_url($url), '@title' => $title)) . '</div>';
}