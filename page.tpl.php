<!DOCTYPE html> 
<html> 
	<head> 
  <title><?php print $head_title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1"> 
  <link rel="stylesheet" href="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
  <?php print $styles; ?>
  <script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
  <?php print $mobile_scripts; ?>
  <script src="http://code.jquery.com/mobile/1.1.1/jquery.mobile-1.1.1.min.js"></script>
</head> 
<body> 

<div data-role="page">

  <div data-role="header" data-theme="b">
    <h1><?php print $site_name; ?></h1>

			<?php if (!$is_front): ?>
      <a href="<?php print $front_page; ?>" data-icon="home" data-iconpos="notext"><?php print t('Home'); ?></a>
      <a href="#" data-rel="back" data-icon="back" data-iconpos="notext" data-direction="reverse"><?php print t('Back'); ?></a>
      <?php endif; ?>

			<?php if (!empty($primary_links)&&arg(1)!='node'): ?>
        <div data-role="navbar">
          <?php print theme('links', $primary_links); ?>
        </div><!-- /navbar -->
      <?php endif; ?>
        
  </div><!-- /header -->

  <div data-role="content">	

    <div class="content-primary">
      <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
      <?php print $content; ?>
    </div><!-- /content-primary -->

    <div class="content-secondary">
      <?php if (!empty($content_secondary)): ?>
          <?php print $content_secondary; ?>
      <?php endif; ?>
    </div><!-- /content-secondary -->

  </div><!-- /content -->

    <div data-role="footer">
      <h4><?php print $footer_message; ?></h4>
    </div><!-- /footer -->  

</div><!-- /page -->

</body>
</html>